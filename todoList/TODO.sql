USE [master]
GO
/****** Object:  Database [TODO]    Script Date: 9/25/2020 10:33:03 AM ******/
CREATE DATABASE [TODO]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'TODO', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.TCSON\MSSQL\DATA\TODO.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'TODO_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.TCSON\MSSQL\DATA\TODO_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [TODO] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [TODO].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [TODO] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [TODO] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [TODO] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [TODO] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [TODO] SET ARITHABORT OFF 
GO
ALTER DATABASE [TODO] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [TODO] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [TODO] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [TODO] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [TODO] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [TODO] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [TODO] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [TODO] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [TODO] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [TODO] SET  DISABLE_BROKER 
GO
ALTER DATABASE [TODO] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [TODO] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [TODO] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [TODO] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [TODO] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [TODO] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [TODO] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [TODO] SET RECOVERY FULL 
GO
ALTER DATABASE [TODO] SET  MULTI_USER 
GO
ALTER DATABASE [TODO] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [TODO] SET DB_CHAINING OFF 
GO
ALTER DATABASE [TODO] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [TODO] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [TODO] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'TODO', N'ON'
GO
ALTER DATABASE [TODO] SET QUERY_STORE = OFF
GO
USE [TODO]
GO
/****** Object:  UserDefinedFunction [dbo].[AUTO_BRY]    Script Date: 9/25/2020 10:33:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[AUTO_BRY]()
RETURNS VARCHAR(5)
AS
BEGIN
	DECLARE @ID VARCHAR(5)
	IF (SELECT COUNT(id) FROM UserList) = 0
		SET @ID = '0'
	ELSE
		SELECT @ID = MAX(RIGHT(id, 2)) FROM UserList
		SELECT @ID = CASE
			WHEN @ID >= 0 and @ID < 9 THEN 'BRY0' + CONVERT(CHAR, CONVERT(INT, @ID) + 1)
			WHEN @ID >= 9 THEN 'BRY0' + CONVERT(CHAR, CONVERT(INT, @ID) + 1)
		END
	RETURN @ID
END
GO
/****** Object:  UserDefinedFunction [dbo].[AUTO_TD]    Script Date: 9/25/2020 10:33:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[AUTO_TD]()
RETURNS VARCHAR(5)
AS
BEGIN
	DECLARE @ID VARCHAR(5)
	IF (SELECT COUNT(idList) FROM TodoList) = 0
		SET @ID = '0'
	ELSE
		SELECT @ID = MAX(RIGHT(idList, 3)) FROM TodoList
		SELECT @ID = CASE
			WHEN @ID >= 0 and @ID < 9 THEN 'TD00' + CONVERT(CHAR, CONVERT(INT, @ID) + 1)
			WHEN @ID >= 9 and @ID <99 THEN 'TD0' + CONVERT(CHAR, CONVERT(INT, @ID) + 1)
			WHEN @ID >= 99 THEN 'TD' + CONVERT(CHAR, CONVERT(INT, @ID) + 1)
		END
	RETURN @ID
END
GO
/****** Object:  Table [dbo].[ROLE]    Script Date: 9/25/2020 10:33:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ROLE](
	[idRole] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](20) NOT NULL,
 CONSTRAINT [PK__ROLE__E5045C54CF8EB8A5] PRIMARY KEY CLUSTERED 
(
	[idRole] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[STATUS]    Script Date: 9/25/2020 10:33:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[STATUS](
	[idStatus] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
 CONSTRAINT [PK__STATUS__01936F74151D2094] PRIMARY KEY CLUSTERED 
(
	[idStatus] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TodoList]    Script Date: 9/25/2020 10:33:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TodoList](
	[idList] [char](5) NOT NULL,
	[content] [nvarchar](200) NOT NULL,
	[expiredDate] [date] NULL,
	[deleted] [int] NULL,
	[idUser] [char](5) NULL,
	[idStatus] [int] NULL,
 CONSTRAINT [PK__TodoList__143D7F07ED0AF009] PRIMARY KEY CLUSTERED 
(
	[idList] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserList]    Script Date: 9/25/2020 10:33:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserList](
	[id] [char](5) NOT NULL,
	[email] [varchar](50) NOT NULL,
	[password] [varchar](50) NOT NULL,
	[fullName] [nvarchar](50) NULL,
	[dob] [date] NULL,
	[tel] [varchar](20) NULL,
	[deleted] [int] NULL,
	[idRole] [int] NULL,
 CONSTRAINT [PK__UserList__3213E83FCDEF0892] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[ROLE] ON 

INSERT [dbo].[ROLE] ([idRole], [name]) VALUES (1, N'Admin')
INSERT [dbo].[ROLE] ([idRole], [name]) VALUES (2, N'User')
SET IDENTITY_INSERT [dbo].[ROLE] OFF
GO
SET IDENTITY_INSERT [dbo].[STATUS] ON 

INSERT [dbo].[STATUS] ([idStatus], [name]) VALUES (1, N'Progressing')
INSERT [dbo].[STATUS] ([idStatus], [name]) VALUES (2, N'Done')
INSERT [dbo].[STATUS] ([idStatus], [name]) VALUES (3, N'Canceled')
SET IDENTITY_INSERT [dbo].[STATUS] OFF
GO
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD001', N'đi chơi trung thu', CAST(N'2020-09-25' AS Date), 0, N'BRY03', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD002', N'đi ăn Shushi cc', CAST(N'2020-09-09' AS Date), 0, N'BRY02', 2)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD003', N'làm bài tập', CAST(N'2020-09-30' AS Date), 1, N'BRY02', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD004', N'đi ăn Sushi', CAST(N'2020-09-10' AS Date), 0, N'BRY01', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD005', N'Chơi thể thao', CAST(N'2020-09-23' AS Date), 0, N'BRY02', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD006', N'chơi điện tử thùng', CAST(N'2020-09-03' AS Date), 0, N'BRY02', 2)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD007', N'ăn Khuya', CAST(N'2020-09-04' AS Date), 0, N'BRY02', 3)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD008', N'hoàn thành project', CAST(N'2020-09-15' AS Date), 0, N'BRY02', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD009', N'đi chơi PS4', CAST(N'2020-09-16' AS Date), 1, N'BRY01', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD010', N'đi mua PS5', CAST(N'2020-11-19' AS Date), 0, N'BRY01', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD011', N'Chơi thể thao', CAST(N'2020-09-18' AS Date), 0, N'BRY01', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD012', N'chơi điện tử thùng', CAST(N'2020-09-09' AS Date), 0, N'BRY01', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD013', N'an khuya', CAST(N'2020-09-30' AS Date), 0, N'BRY01', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD014', N'kajasd', CAST(N'2020-09-30' AS Date), 0, N'BRY01', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD015', N'hoàn thành project', CAST(N'2020-09-07' AS Date), 0, N'BRY01', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD016', N'thức', CAST(N'2020-09-02' AS Date), 0, N'BRY01', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD017', N'qưdqwqd', CAST(N'2020-09-15' AS Date), 0, N'BRY01', 3)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD018', N'thức không ngủ để...', CAST(N'2020-09-11' AS Date), 0, N'BRY01', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD019', N'せかい守る', CAST(N'2020-09-11' AS Date), 0, N'BRY01', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD020', N'chơi đá banh', CAST(N'2020-09-12' AS Date), 0, N'BRY01', 2)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD021', N'nani nani', CAST(N'2020-09-21' AS Date), 0, N'BRY01', 2)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD022', N'Săn Kaiju dsssdsd', CAST(N'2020-09-17' AS Date), 0, N'BRY01', 2)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD023', N'chơi Assassin''s Creed', CAST(N'2020-09-30' AS Date), 0, N'BRY01', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD024', N'ăn kem　あひひお', CAST(N'2020-09-10' AS Date), 0, N'BRY01', 3)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD025', N'Đi thuyền Rồng', CAST(N'2020-09-04' AS Date), 0, N'BRY01', 2)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD026', N'đi Đông Bar', CAST(N'2020-09-12' AS Date), 1, N'BRY01', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD027', N'đi chơi PS4', CAST(N'2020-09-02' AS Date), 0, N'BRY03', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD028', N'hello', CAST(N'2020-09-08' AS Date), 0, N'BRY02', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD029', N'cuowngf', CAST(N'2020-09-05' AS Date), 0, N'BRY02', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD030', N'đi ăn Shushi', CAST(N'2020-09-01' AS Date), 0, N'BRY02', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD031', N'di choi', CAST(N'2020-09-08' AS Date), 1, N'BRY02', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD032', N'di choi', CAST(N'2020-09-30' AS Date), 0, N'BRY02', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD033', N'hehehe', CAST(N'2020-09-18' AS Date), 1, N'BRY02', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD034', N'hehehe', CAST(N'2020-09-18' AS Date), 0, N'BRY02', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD035', N'hehehe', CAST(N'2020-09-18' AS Date), 0, N'BRY02', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD036', N'hehehe', CAST(N'2020-09-18' AS Date), 0, N'BRY02', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD037', N'hehehe', CAST(N'2020-09-18' AS Date), 0, N'BRY02', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD038', N'hehehe', CAST(N'2020-09-18' AS Date), 0, N'BRY02', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD039', N'hehehe', CAST(N'2020-09-18' AS Date), 0, N'BRY02', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD040', N'hehehe', CAST(N'2020-09-18' AS Date), 0, N'BRY02', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD041', N'hehehe', CAST(N'2020-09-18' AS Date), 0, N'BRY02', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD042', N'hehehe', CAST(N'2020-09-18' AS Date), 0, N'BRY02', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD043', N'hehehe', CAST(N'2020-09-18' AS Date), 0, N'BRY02', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD044', N'hehehe', CAST(N'2020-09-18' AS Date), 0, N'BRY02', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD045', N'hehehe', CAST(N'2020-09-18' AS Date), 1, N'BRY02', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD046', N'hehehe', CAST(N'2020-09-18' AS Date), 1, N'BRY02', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD047', N'hehehe', CAST(N'2020-09-18' AS Date), 1, N'BRY02', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD048', N'hehehe', CAST(N'2020-09-18' AS Date), 1, N'BRY02', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD049', N'dewfcwefw', CAST(N'2020-09-02' AS Date), 1, N'BRY02', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD050', N'dewfcwefw', CAST(N'2020-09-02' AS Date), 1, N'BRY02', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD051', N'ăn kem', CAST(N'2020-09-18' AS Date), 0, N'BRY02', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD052', N'Chơi thể thao', CAST(N'2020-09-04' AS Date), 0, N'BRY02', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD053', N'dwdwdw', CAST(N'2020-09-03' AS Date), 1, N'BRY01', 3)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD054', N'Chơi thể thao thể dục', CAST(N'2020-09-09' AS Date), 0, N'BRY02', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD055', N'ăn ốc', CAST(N'2020-09-15' AS Date), 0, N'BRY02', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD056', N'ăn lẩu', CAST(N'2020-09-03' AS Date), 0, N'BRY02', 3)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD057', N'ăn kem Que', CAST(N'2020-09-15' AS Date), 1, N'BRY01', 1)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD058', N'vui vẻ k quạu nha', CAST(N'2020-09-04' AS Date), 0, N'BRY01', 2)
INSERT [dbo].[TodoList] ([idList], [content], [expiredDate], [deleted], [idUser], [idStatus]) VALUES (N'TD059', N'ăn kem', CAST(N'2020-09-22' AS Date), 0, N'BRY01', 2)
GO
INSERT [dbo].[UserList] ([id], [email], [password], [fullName], [dob], [tel], [deleted], [idRole]) VALUES (N'BRY01', N'roonysilver@gmail.com', N'e10adc3949ba59abbe56e057f20f883e', N'JASON CHEN', CAST(N'1991-01-11' AS Date), N'0221245215', 0, 1)
INSERT [dbo].[UserList] ([id], [email], [password], [fullName], [dob], [tel], [deleted], [idRole]) VALUES (N'BRY02', N'tc_son@gmail.com', N'e10adc3949ba59abbe56e057f20f883e', N'Tran Cong Son', CAST(N'1995-08-11' AS Date), N'0122321215', 0, 2)
INSERT [dbo].[UserList] ([id], [email], [password], [fullName], [dob], [tel], [deleted], [idRole]) VALUES (N'BRY03', N'lycu6565@gmail.com', N'e10adc3949ba59abbe56e057f20f883e', N'Son Tran Cong', CAST(N'1995-10-15' AS Date), N'0906332014', 0, 2)
GO
ALTER TABLE [dbo].[TodoList] ADD  CONSTRAINT [DF_TodoList_idList]  DEFAULT ([DBO].[AUTO_TD]()) FOR [idList]
GO
ALTER TABLE [dbo].[TodoList] ADD  CONSTRAINT [DF__TodoList__delete__38996AB5]  DEFAULT ((0)) FOR [deleted]
GO
ALTER TABLE [dbo].[UserList] ADD  CONSTRAINT [DF_UserList_id]  DEFAULT ([DBO].[AUTO_BRY]()) FOR [id]
GO
ALTER TABLE [dbo].[UserList] ADD  CONSTRAINT [DF__UserList__delete__3A81B327]  DEFAULT ((0)) FOR [deleted]
GO
ALTER TABLE [dbo].[TodoList]  WITH CHECK ADD  CONSTRAINT [FK__TodoList__idStat__403A8C7D] FOREIGN KEY([idStatus])
REFERENCES [dbo].[STATUS] ([idStatus])
GO
ALTER TABLE [dbo].[TodoList] CHECK CONSTRAINT [FK__TodoList__idStat__403A8C7D]
GO
ALTER TABLE [dbo].[TodoList]  WITH CHECK ADD  CONSTRAINT [FK__TodoList__idUser__398D8EEE] FOREIGN KEY([idUser])
REFERENCES [dbo].[UserList] ([id])
GO
ALTER TABLE [dbo].[TodoList] CHECK CONSTRAINT [FK__TodoList__idUser__398D8EEE]
GO
ALTER TABLE [dbo].[UserList]  WITH CHECK ADD  CONSTRAINT [FK__UserList__idRole__412EB0B6] FOREIGN KEY([idRole])
REFERENCES [dbo].[ROLE] ([idRole])
GO
ALTER TABLE [dbo].[UserList] CHECK CONSTRAINT [FK__UserList__idRole__412EB0B6]
GO
/****** Object:  StoredProcedure [dbo].[SP_GET_COUNT_RECORD]    Script Date: 9/25/2020 10:33:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SP_GET_COUNT_RECORD]
@idUser CHAR(5)
AS
BEGIN
SELECT COUNT(*) AS Count FROM TodoList WHERE idUser=@idUser AND deleted =0
END
GO
/****** Object:  StoredProcedure [dbo].[SP_GET_TODO]    Script Date: 9/25/2020 10:33:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SP_GET_TODO]
@expiredDate AS INT,
@idUser AS CHAR(5),
@PageNumber AS INT,
@PageSize AS INT,
@RowNum AS INT
AS
BEGIN
SELECT * FROM(
SELECT ROW_NUMBER()OVER(Order by (select(0))) as rownum,TodoList.*,STATUS.name FROM TodoList JOIN STATUS ON TodoList.idStatus = STATUS.idStatus WHERE deleted = 0 AND idUser=@idUser
) AS t 
ORDER BY 
(CASE @RowNum WHEN 1 THEN rownum END) DESC,
(CASE @RowNum WHEN 2 THEN rownum END) ASC,
(CASE @expiredDate WHEN 1 THEN 1 END),
(CASE @expiredDate WHEN 2 THEN t.expiredDate END) DESC,
(CASE @expiredDate WHEN 3 THEN t.expiredDate END) ASC
OFFSET ((@PageNumber - 1) * @PageSize) ROWS
FETCH NEXT @PageSize ROWS ONLY;
END
GO
/****** Object:  StoredProcedure [dbo].[SP_GET_USER]    Script Date: 9/25/2020 10:33:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SP_GET_USER]
AS
BEGIN
SELECT * FROM [UserList] JOIN [ROLE] ON [UserList].idRole = [ROLE].idRole WHERE deleted = 0 AND [UserList].idRole=2
END
GO
/****** Object:  StoredProcedure [dbo].[SP_GET_USERLIST]    Script Date: 9/25/2020 10:33:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SP_GET_USERLIST]
@email NVARCHAR(50),
@password NVARCHAR(50)
AS
BEGIN
SELECT * FROM UserList JOIN ROLE ON UserList.idRole = ROLE.idRole WHERE email=@email AND password=@password
END
GO
USE [master]
GO
ALTER DATABASE [TODO] SET  READ_WRITE 
GO
