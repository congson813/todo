﻿Public Interface ITodoList
    Function GetTodo(ByVal todo As TodoList) As TodoList
    Function GetListTodo(ByVal idUser As String, ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal ExpiredDate As Integer, RowNum As Integer) As List(Of TodoList)
    Function DeleteTodo(id As String) As Boolean
    Function UpdateTodo(todo As TodoList) As TodoList
    Function CreateTodo(todo As TodoList) As TodoList
    'Function ShortTodo(todo As TodoList) As TodoList
    Function GetCount(ByVal idUser As String) As Integer
End Interface
