﻿Imports System.Data.SqlClient

Public Class TodoListService
    Implements ITodoList
    ''Connect With Database
    Dim connectionString As String = ConfigurationManager.ConnectionStrings("myConnectionString").ConnectionString
    Dim con As New SqlConnection(connectionString)
    Public Function GetTodo(todo As TodoList) As TodoList Implements ITodoList.GetTodo
        Throw New NotImplementedException()
    End Function
    ''' <summary>
    ''' Get TODO List from DataBase using Stored Procedure
    ''' </summary>
    ''' <param name="idUser"></param>
    ''' <param name="PageNumber"></param>
    ''' <param name="PageSize"></param>
    ''' <param name="ExpiredDate"></param>
    ''' <param name="RowNum"></param>
    ''' <returns></returns>
    Public Function GetListTodo(idUser As String, PageNumber As Integer, PageSize As Integer, ExpiredDate As Integer, RowNum As Integer) As List(Of TodoList) Implements ITodoList.GetListTodo
        Dim data As New List(Of TodoList)
        con.Open()
        Dim cmd = New SqlCommand()
        cmd.CommandText = "EXEC SP_GET_TODO @expiredDate,@idUser,@PageNumber,@PageSize,@RowNum"
        cmd.CommandType = System.Data.CommandType.Text
        cmd.Connection = con
        cmd.Parameters.AddWithValue("@idUser", idUser)
        cmd.Parameters.AddWithValue("@expiredDate", ExpiredDate)
        cmd.Parameters.AddWithValue("@PageNumber", PageNumber)
        cmd.Parameters.AddWithValue("@PageSize", PageSize)
        cmd.Parameters.AddWithValue("@RowNum", RowNum)
        Dim dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Do While dbReader.Read()
            Dim todoObj As New TodoList
            todoObj.IdList = dbReader("idList").ToString
            todoObj.Content = dbReader("content").ToString
            todoObj.ExpiredDate = dbReader("expiredDate").ToString
            todoObj.idStatus = dbReader("idStatus").ToString
            todoObj.IdUser = dbReader("idUser").ToString
            todoObj.name = dbReader("name").ToString
            todoObj.rowNum = dbReader("rownum").ToString
            data.Add(todoObj)
        Loop
        con.Close()
        Return data
    End Function
    ''' <summary>
    ''' Delete To do List. Not compelete delete but change value delete column form 0 to 1
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    Public Function DeleteTodo(id As String) As Boolean Implements ITodoList.DeleteTodo
        Dim rowAffected As Integer = 0
        con.Open()
        Dim cmd = New SqlCommand()
        cmd.CommandText = "UPDATE [TodoList] SET deleted = 1 WHERE [TodoList].idList=@idList"
        cmd.CommandType = System.Data.CommandType.Text
        cmd.Connection = con
        cmd.Parameters.Add(New SqlParameter("@idList", id))
        rowAffected = Int(cmd.ExecuteNonQuery())
        con.Close()
        Return rowAffected
    End Function
    ''' <summary>
    ''' Function update TODO list 
    ''' </summary>
    ''' <param name="todo"></param>
    ''' <returns></returns>
    Public Function UpdateTodo(todo As TodoList) As TodoList Implements ITodoList.UpdateTodo
        Dim rowID As Integer = 0
        con.Open()
        Dim cmd = New SqlCommand()
        cmd.CommandText = "UPDATE [TodoList] SET content=@content,expiredDate=@expiredDate,idStatus=@idStatus WHERE idList=@idList SELECT @@IDENTITY"
        cmd.CommandType = System.Data.CommandType.Text
        cmd.Connection = con
        cmd.Parameters.Add(New SqlParameter("@idList", todo.IdList))
        cmd.Parameters.Add(New SqlParameter("@content", todo.Content))
        cmd.Parameters.Add(New SqlParameter("@expiredDate", todo.ExpiredDate))
        cmd.Parameters.Add(New SqlParameter("@idStatus", todo.idStatus))
        cmd.ExecuteScalar()
        con.Close()
        Return todo
    End Function
    ''' <summary>
    ''' Function Create TODO List
    ''' </summary>
    ''' <param name="todo"></param>
    ''' <returns>a row will be added to table</returns>
    Public Function CreateTodo(todo As TodoList) As TodoList Implements ITodoList.CreateTodo
        Dim TodoId As Integer = 0
        con.Open()
        Dim cmd = New SqlCommand()
        cmd.CommandText = "INSERT INTO [TodoList] (content,expiredDate,idStatus,idUser) VALUES (@content,@expiredDate,@idStatus,@idUser)"
        cmd.CommandType = System.Data.CommandType.Text
        cmd.Connection = con
        cmd.Parameters.Add(New SqlParameter("@content", todo.Content))
        cmd.Parameters.Add(New SqlParameter("@expiredDate", todo.ExpiredDate))
        cmd.Parameters.Add(New SqlParameter("@idStatus", todo.idStatus))
        cmd.Parameters.Add(New SqlParameter("@idUser", todo.IdUser))
        cmd.ExecuteScalar()
        con.Close()
        Return todo
    End Function
    ''' <summary>
    ''' Count Rows of idUser From Todo to split TODOUSER 
    ''' </summary>
    ''' <param name="idUser"></param>
    ''' <returns>count every idUser in Todo list</returns>
    Public Function GetCount(idUser As String) As Integer Implements ITodoList.GetCount
        Dim intCountRecord As Integer
        con.Open()
        Dim cmd = New SqlCommand()
        cmd.CommandText = "SP_GET_COUNT_RECORD @idUser"
        cmd.CommandType = System.Data.CommandType.Text
        cmd.Connection = con
        cmd.Parameters.Add(New SqlParameter("@idUser", idUser))
        Dim dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        dbReader.Read()
        intCountRecord = dbReader("Count").ToString
        con.Close()
        Return intCountRecord
    End Function
End Class
